<?php

namespace App\Timetable\Model;

use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;
use Vascowhite\Time\TimeValue;

class Line
{
    /**
     * @JMS\Type("App\Timetable\Model\Variant")
     */
    public $variants;
    /**
     * @JMS\Type("array<integer, ArrayCollection>")
     */
    public $departures;

    /**
     * @param $variants
     * @param $departures
     */
    public function __construct($variants, $departures)
    {
        $this->variants = $variants;
        $this->departures = $departures;
    }

    /**
     * @return ArrayCollection
     */
    public function getVariants()
    {
        return $this->variants;
    }

    /**
     * @return ArrayCollection
     */
    public function getDepartures()
    {
        return $this->departures;
    }

    public function getTimetable($stopId): array
    {
        $timetable = [];
        foreach ($this->getDepartures() as $variantId => $departure) {
            foreach ($departure as $departureTime) {
                $departureTime = new TimeValue($departureTime, "H:i");

                $travelDuration = $this->getVariantTravelDuration($variantId, $stopId);
                if ($travelDuration != null) {
                    $stopDepartureTime = $departureTime->add(new TimeValue($travelDuration, "i"));
                    $timetable[] = $stopDepartureTime;
                }
            }
        }

        return $timetable;
    }

    /**
     * @param int $variantId
     * @param int $stopId
     * @return string
     */
    private function getVariantTravelDuration(int $variantId, int $stopId): ?string
    {
        /** @var Variant $variant */
        foreach ($this->getVariants() as $variant) {
            if ($variant->getId() == $variantId) {
                foreach ($variant->getStops() as $stop) {
                    if ($stopId == $stop->getId()) {
                        return $stop->getMinutesFromStart();
                    }
                }
            }
        }

        return null;
    }
}