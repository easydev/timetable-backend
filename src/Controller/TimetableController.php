<?php

namespace App\Controller;

use App\Timetable\TimetableProviderInterface;
use App\Timetable\TimetableService;
use JMS\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class TimetableController extends Controller
{

    /**
     * @var TimetableProviderInterface
     */
    private $timetableProvider;
    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(TimetableService $timetableProvider, SerializerInterface $serializer)
    {
        $this->timetableProvider = $timetableProvider;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/")
     */
    public function index()
    {
        $stops = $this->timetableProvider->findAllStops();

        return $this->createJsonResponse($stops);
    }

    /**
     * @Route("/stop/{stopId}")
     * @param $stopId
     * @return Response
     */
    public function stopTimetable($stopId)
    {
        $lines = $this->timetableProvider->findTimetableForStopId($stopId);

        return $this->createJsonResponse($lines);
    }

    /**
     * @Route("/departures/stop/{stopId}/{timeHours}/{timeMinutes}")
     */
    public function stopDeparturesSince($stopId, $timeHours, $timeMinutes)
    {
        $departures = $this->timetableProvider->findDeparturesSince($stopId, $timeHours, $timeMinutes);

        return $this->createJsonResponse($departures);
    }

    /**
     * @param $lines
     * @return Response
     */
    private function createJsonResponse($data): Response
    {
        $response = new Response($this->serializer->serialize($data, 'json'));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}