
Installation 
-------------

    composer install
    bin/console server:run
    
Tests
------
   
 phpspec:
   
    vendor/bin/phpspec run 
   
 behat:
   
    bin/console server:run
    vendor/bin/behat
    
Features
---------

Available endpoints:

* index
    
        GET /
    
    provides list of available stops 
  
  
* timetable for stop
    
        GET /stop/{stopId}
        
  provides detail timetable for stop
  
  
  
* departures list for stopId and 

        GET /departures/stop/{stopId}/{timeHours}/{timeMinutes}
 
Response :

        [
            {
                "line": 2,                    // Line number
                "departure_time": "12:00:00", // departure time
                "human_departure_time": "15m" // human readable departure time
            }
         ]
            
        
TODO
* optimize algorithm to calculate departure list only for requested period of time
* input validation