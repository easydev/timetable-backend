<?php

namespace App\Timetable\Model;

class VariantStop
{
    /**
     * @var integer
     */
    public $id;
    /**
     * @var integer
     */
    public $minutesFromStart;

    /**
     * VariantStop constructor.
     * @param int $id
     * @param int $minutesFromStart
     */
    public function __construct(int $id, int $minutesFromStart)
    {
        $this->id = $id;
        $this->minutesFromStart = $minutesFromStart;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getMinutesFromStart(): int
    {
        return $this->minutesFromStart;
    }
}