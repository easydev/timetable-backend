<?php

namespace App\Timetable\Model;

use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;

class Timetable
{
    /**
     * @JMS\Type("App\Timetable\Model\Stop")
     */
    public $stops;
    /**
     * @JMS\Type("ArrayCollection<integer, App\Timetable\Model\Line>")
     */
    public $lines;

    public function __construct(ArrayCollection $stops, ArrayCollection $lines)
    {
        $this->stops = $stops;
        $this->lines = $lines;
    }

    /**
     * @return ArrayCollection<Stops>
     */
    public function getStops(): ArrayCollection
    {
        return $this->stops;
    }

    /**
     * @return ArrayCollection
     */
    public function getLines()
    {
        return $this->lines;
    }

    /**
     * @param int $stopId
     * @return ArrayCollection
     */
    public function getStopTimetable(int $stopId): ArrayCollection
    {
        $stopLines = new ArrayCollection();
        /** @var Line $details */
        foreach ($this->getLines() as $lineId => $line) {
            /** @var Variant $variant */
            foreach ($line->getVariants() as $variant) {
                /** @var VariantStop $stop */
                foreach ($variant->getStops() as $stop) {
                    if ($stop->getId() == $stopId) {
                        $stopLines[$lineId] = $line->getTimetable($stopId);
                    }
                }
            }
        }

        return $stopLines;
    }
}