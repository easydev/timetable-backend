<?php

namespace App\Timetable;

use App\Timetable\Model\Timetable;
use Doctrine\Common\Collections\ArrayCollection;

interface TimetableProviderInterface
{
    public function findAllStops(): ArrayCollection;

    public function findTimetableForStopId(int $stopId): ArrayCollection;

    public function findStopData(int $stopId): Timetable;
}