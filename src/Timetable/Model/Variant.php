<?php

namespace App\Timetable\Model;

use Doctrine\Common\Collections\ArrayCollection;

class Variant
{
    /**
     * @var integer
     */
    public $id;
    /**
     * @var ArrayCollection<VariantStop>
     */
    public $stops;

    /**
     * Variants constructor.
     * @param int $id
     * @param ArrayCollection $stops
     */
    public function __construct(int $id, ArrayCollection $stops)
    {
        $this->id = $id;
        $this->stops = $stops;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return ArrayCollection<VariantStop>
     */
    public function getStops(): ArrayCollection
    {
        return $this->stops;
    }
}