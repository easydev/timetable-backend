<?php
namespace App\Timetable\JMSeriallizer;

use App\Timetable\Model\Stop;
use App\Timetable\Model\Variant;
use App\Timetable\Model\VariantStop;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\VisitorInterface;

class VariantsHandler implements SubscribingHandlerInterface
{
    /**
     * @return array
     */
    public static function getSubscribingMethods()
    {
        return [
            array(
                'direction' => GraphNavigator::DIRECTION_DESERIALIZATION,
                'format' => 'json',
                'type' => Variant::class,
                'method' => 'deserializeField',
            ),
        ];
    }

    public function deserializeField(VisitorInterface $visitor, $data, array $type, Context $context)
    {
        $variants = [];
        foreach ($data as $id => $variantData) {
            $variants[] = new Variant($id, $this->getStops($variantData));
        }

        return new ArrayCollection($variants);
    }

    private function getStops(array $variantData) : ArrayCollection
    {
        $collection = new ArrayCollection();
        foreach ($variantData as $variant) {
            $collection[] = new VariantStop(key($variant),reset($variant));
        }
        return $collection;
    }
}