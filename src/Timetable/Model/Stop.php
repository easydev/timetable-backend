<?php
namespace App\Timetable\Model;

class Stop
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $stopName;


    public function __construct(int $id, string $stopName)
    {
        $this->id = $id;
        $this->stopName = $stopName;
    }

    /**
     * @return int
     */
    public function getStopId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getStopName() : string
    {
        return $this->stopName;
    }
}