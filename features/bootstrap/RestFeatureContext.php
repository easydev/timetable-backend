<?php

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use GuzzleHttp\Exception\BadResponseException;
use Hamcrest\MatcherAssert as ha;
use Hamcrest\Matchers as hm;
use Psr\Http\Message\ResponseInterface;

class RestFeatureContext implements Context, SnippetAcceptingContext
{
    use JsonContextTrait;

    /** @var ResponseInterface */
    protected $response;
    /** @var \GuzzleHttp\Client */
    protected $client;

    /**
     * @param array $parameters
     */
    public function __construct()
    {
        $this->client = new GuzzleHttp\Client(
            ['base_uri' => "http://localhost:8000", 'verify' => false, 'allow_redirects' => true]
        );
    }

    /**
     * @return ResponseInterface
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @When /^(?:I )?send a ([A-Z]+) request to "([^"]+)" with body:$/
     * @param              $method
     * @param              $url
     * @param PyStringNode $body
     */
    public function iSendARequestWithBody($method, $url, PyStringNode $body)
    {
        $contentValue = $this->buildMultipart($body->getStrings());
        $this->sendARequestWithBodyPair($method, $url, 'multipart', $contentValue);
    }

    /**
     * @When /^(?:I )?send a ([A-Z]+) request to "([^"]+)" with JSON body:$/
     * @param              $method
     * @param              $url
     * @param String       $body
     */
    public function iSendARequestWithJSONBody($method, $url, PyStringNode $body)
    {
        $contentValue = $body->getRaw();
        $this->sendARequestWithBodyPair($method, $url, 'json', $contentValue);
    }

    /**
     * @param              $method
     * @param              $url
     * @param              $contentTypeKey
     * @param              $contentValue
     */
    public function sendARequestWithBodyPair($method, $url, $contentTypeKey, $contentValue)
    {
        try {
            $this->response = $this->client->request($method, $url, array(
                $contentTypeKey => $contentValue,
                'query' => [
                    'XDEBUG_SESSION_START' => time(),
                    'env' => 'behat'
                ]
            ));
        } catch (BadResponseException $e) {
            $this->response = $e->getResponse();
            if (null === $this->response) {
                throw $e;
            }
        }
    }

    /**
     * @When /^(?:I )?send a ([A-Z]+) request to "([^"]+)"$/
     * @param $method
     * @param $url
     */
    public function iSendARequest($method, $url)
    {
        $query = [
            'XDEBUG_SESSION_START' => time(),
            'env' => 'behat'
        ];

        if ('GET' == $method && strpos($url, '?')) {
            $url = explode('?', $url);
            $params = $url[1];
            $url = $url[0];

            $params = explode('&', $params);
            foreach ($params as $param) {
                $param = explode('=', $param);
                $query[$param[0]] = $param[1];
            }
        }

        try {
            $this->response = $this->client->request($method, $url, array(
                'query' => $query
            ));
        } catch (BadResponseException $e) {
            $this->response = $e->getResponse();
            if (null === $this->response) {
                throw $e;
            }
        }
    }

    /**
     * @Then I print response
     */
    public function iPrintResponse()
    {
        $response = $this->getResponse();

        echo sprintf(
            "%s",
            $response->getBody(true)
        );
    }

    /**
     * @Then I get :status status
     * @param $httpStatusCode
     */
    public function iGetStatus($httpStatusCode)
    {
        $responseStatusCode = (int)$this->getResponse()->getStatusCode();
        ha::assertThat(
            "Returned " . PHP_EOL . "-----------" . PHP_EOL .
            strip_tags($this->getResponse()->getBody(true)) . PHP_EOL,
            $responseStatusCode,
            hm::identicalTo((int)$httpStatusCode)
        );

        $this->responseDoesNotHaveKey("error");
    }

    /**
     * @Then I get Response header :arg1 with value :arg2
     */
    public function iGetResponseHeaderWithValue($arg1, $arg2)
    {
        $responseHeaders = $this->getResponse()->getHeaders();
        if ($responseHeaders[$arg1][0] != $arg2) {
            throw new \Exception('Response Header "'.$arg1.'" value is "'.$responseHeaders[$arg1][0]);
        }
    }

    /**
     * @param string $name
     * @param string $value
     */
    public function addRequestHeader($name, $value)
    {
        $this->getKernel()->getContainer()->get('crv.test_headers_bag')->set($name, $value);
    }

    /**
     * @param string $name
     */
    public function removeRequestHeader($name)
    {
        $this->getKernel()->getContainer()->get('crv.test_headers_bag')->remove($name);
    }

    /**
     * @param $parameters
     * @return array
     */
    private function buildMultipart(array $parameters)
    {
        $multipartParameters = [];
        foreach ($parameters as $nameValue) {
            list($name, $value) = explode('=', $nameValue);
            if($value || $value === '0') {
                $value = trim($value);
            } else {
                $value = null;
            }
            $multipartParameters[] = ['name' => trim($name), 'contents' => $value];
        }

        return $multipartParameters;
    }

    /**
     * @Given /^feature "(?<featureName>[A-Za-z\-]+)" is on$/
     */
    public function featureIsOn($featureName)
    {
        $this->addRequestHeader($featureName, 1);
    }

    /**
     * @Given /^feature "(?<featureName>[A-Za-z\-]+)" is off$/
     */
    public function featureIsOff($featureName)
    {
        $this->addRequestHeader($featureName, 0);
    }

    /**
     * @Given /^user agent "([^"]+)"$/
     */
    public function userAgent($userAgent)
    {
        $this->addRequestHeader('User-Agent', $userAgent);
    }

    /**
     * @Then the response status code should be :code
     */
    public function theResponseStatusCodeShouldBe($code)
    {
        return $this->response->getStatusCode() === $code;
    }

    /**
     * @Then the response status code should not be :code
     */
    public function theResponseStatusCodeShouldNotBe($code)
    {
        return $this->response->getStatusCode() !== $code;
    }
}