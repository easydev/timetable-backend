<?php
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Hamcrest\MatcherAssert as ha;
use Hamcrest\Matchers as hm;

trait JsonContextTrait
{
    /**
     * @Given /^the JSON should be equal to:$/
     *
     * @throws Exception
     *
     * @param PyStringNode $expectedJsonValue
     */
    public function theJsonShouldBeEqualTo(PyStringNode $expectedJsonValue)
    {
        ha::assertThat((String)$this->getResponse()->getBody(true), hm::equalToIgnoringWhiteSpace((String)$expectedJsonValue));
    }

    /**
     * Checks, that given JSON key exist
     *
     * @Given /^the JSON key "([^"]*)" should exist$/
     * @param $name
     */
    public function theJsonNodeShouldExist($name)
    {
        $response = $this->getJsonResponse();

        ha::assertThat($this->isKeyInResponse($response, $name), hm::is(true));
    }

    /**
     * Tries to find key in JSON response
     * @param array $response JSON response
     * @param string $keys Key
     * @param string $delimiter Delimiter
     * @return bool Key found
     */
    private function isKeyInResponse(array $response, $keys = '', $delimiter = '.') {
        if (empty($response) || empty($keys)) {
            return false;
        }

        // In case there is an only a key in root (not deep key)
        if (!strpos($keys, $delimiter)) {
            return array_key_exists($keys, $response);
        }

        // In case there are some deep keys (like data.error or data.error.reason)
        if (preg_match('/([^\.]+)(.*)/i', $keys, $deepKeys)) {
            $key = $deepKeys[1]; // data
            $nextKeys = ltrim($deepKeys[2], '.'); // .error

            return $this->isKeyInResponse($response[$key], $nextKeys);
        }

        return false;
    }

    /**
     * Checks, that given JSON key does not exist
     *
     * @Given /^the JSON key "([^"]*)" should not exist$/
     * @param $name
     */
    public function theJsonNodeShouldNotExist($name)
    {
        $response = $this->getJsonResponse();

        ha::assertThat($this->isKeyInResponse($response, $name), hm::is(false));
    }

    /**
     * @Given /^the JSON response key "([^"]*)" is "([^"]*)"$/
     * @param $name
     * @param $value
     */
    public function responseKeyIs($name, $value)
    {
        $response = $this->getJsonResponse();

        ha::assertThat($response[$name], hm::is(hm::equalTo($this->convertValueToCorrectType($value))));
    }

    /**
     * @Given /^response does not have key "([^"]*)"$/
     * @param $name
     */
    public function responseDoesNotHaveKey($name)
    {
        $response = $this->getJsonResponse();
        if ($response == "") return;
        ha::assertThat($response, hm::notSet($name));
    }

    /**
     * @Then the JSON key :arg1 should be equal to :arg2
     */
    public function theJsonKeyShouldShouldBeEqualTo($arg1, $arg2)
    {
        $response = $this->getJsonResponse();

        ha::assertThat($response[$arg1], hm::equalTo($this->convertValueToCorrectType($arg2)));
    }

    /**
     * @return array
     */
    private function getJsonResponse()
    {
        return json_decode((String)$this->getResponse()->getBody(true), true);
    }

    /**
     * @Then /^I get (\d+) error$/
     */
    public function iGetError($errorCode)
    {
        $response = $this->getJsonResponse();
        ha::assertThat((int)$this->getResponse()->getStatusCode(), hm::equalTo(200));
        ha::assertThat($response, hm::hasKey('error'));
        ha::assertThat($response['error'], hm::hasEntry('code', $errorCode));
        ha::assertThat($response['error'], hm::hasKey('title'));
        ha::assertThat($response['error'], hm::hasKey('detail'));
    }

    /**
     * @Given /^the JSON key "([^"]*)" should exist and contain$/
     */
    public function theJSONKeyShouldExistAndContain($keyName, TableNode $table)
    {
        $values = $table->getColumn(0);

        $json = $this->getJsonResponse();

        ha::assertThat($json, hm::hasKey($keyName));

        foreach ($values as $value) {
            ha::assertThat($json[$keyName], hm::hasItemInArray($value));
        }
    }

    /**
     * @Given /^the JSON object "([^"]*)" should contain:$/
     */
    public function theJSONObjectShouldContain($objectKey, TableNode $table)
    {
        $json = $this->getJsonResponse();

        ha::assertThat($json, hm::hasKey($objectKey));
        foreach ($table->getColumnsHash()[0] as $key => $value) {
            ha::assertThat($json[$objectKey][$key], hm::equalTo($value));
        }
    }

    /**
    * @Given /^the JSON response should contain:$/
    */
    public function theJSONResponseShouldContain(TableNode $table)
    {
       $json = $this->getJsonResponse();

       foreach ($this->getTableHash($table)[0] as $key => $value) {
           ha::assertThat($json[$key], hm::equalTo($value));
       }
    }

    /**
     * Gets table hash and applies type conversions
     *
     * @param Behat\Gherkin\Node\TableNode $table
     * @return array
     */
    private function getTableHash(TableNode $table)
    {
        $hash = $table->getHash();
        foreach ($hash as &$row) {
            foreach ($row as &$value) {
                $value = $this->convertValueToCorrectType($value);
            }
        }
        return $hash;
    }

    /**
     * @param $value
     * @return bool|float|int|null
     */
    private function convertValueToCorrectType($value)
    {
        if ($value === 'false') {
            $value = false;
        } elseif ($value === 'true') {
            $value = true;
        } elseif ($value === '' || $value == 'null') {
            $value = null;
        } elseif (is_numeric($value) && false !== strpos($value, '.')) {
            $value = (float)$value;
        } elseif (is_numeric($value)) {
            $value = (int)$value;
        }

        return $value;
    }

    /**
     * @Then /^the JSON response should be:$/
     */
    public function theJSONResponseShouldBe(PyStringNode $string)
    {
        $input = $string->getRaw();
        if ($input == "{}") { // Prevent conversion of {} to []
            $expected = $input;
        } else {
            $json = json_decode($input, true);
            $expected = json_encode($json);
        }

        $real = (string) $this->getResponse()->getBody(true);

        ha::assertThat(
            $real,
            hm::equalTo($expected)
        );
    }
}
