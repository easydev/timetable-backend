<?php

namespace App\Timetable;

use App\Timetable\Model\Timetable;
use JMS\Serializer\SerializerInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Khill\Duration\Duration;
use Vascowhite\Time\TimeValue;

class TimetableService
{
    /**
     * @var TimetableProviderInterface
     */
    private $timetableProvider;

    public function __construct(TimetableProviderInterface $timetableProvider)
    {
        $this->timetableProvider = $timetableProvider;
    }

    public function findAllStops(): ArrayCollection
    {
        return $this->timetableProvider->findAllStops();
    }

    public function findTimetableForStopId(int $stopId): ArrayCollection
    {
        return $this->timetableProvider->findTimetableForStopId($stopId);
    }

    public function findDeparturesSince(int $stopId, int $timeHours, int $timeMinutes): array
    {
        $data = $this->timetableProvider->findStopData($stopId);
        $departuresInTwoHours = [];
        $fromTime = new TimeValue(sprintf("%d:%d", $timeHours, $timeMinutes), "H:i");
        $maxTime = $fromTime->add(new TimeValue("2:0", "H:i"));

        $timetables = $data->getStopTimetable($stopId);

        foreach ($timetables as $line => $timetable) {
            foreach ($timetable as $departureTime) {
                if ($departureTime->getSeconds() >= $fromTime->getSeconds()
                    && $departureTime->getSeconds() <= $maxTime->getSeconds()
                ) {
                    $humanReadable = (new Duration((new TimeValue($departureTime->sub($fromTime)))->getSeconds()))->humanize();
                    $departuresInTwoHours[] = [
                        'line' => $line,
                        'departure_time' => $departureTime,
                        'human_departure_time' => ($humanReadable) ? $humanReadable : 'due',
                    ];
                }
            }
        }

        usort($departuresInTwoHours, function ($departureA, $departureB) {
            return $departureA['departure_time']->getSeconds() <=> $departureB['departure_time']->getSeconds();
        });

        return $departuresInTwoHours;
    }
}