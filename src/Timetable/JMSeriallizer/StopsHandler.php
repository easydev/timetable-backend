<?php
namespace App\Timetable\JMSeriallizer;

use App\Timetable\Model\Stop;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\VisitorInterface;

class StopsHandler implements SubscribingHandlerInterface
{
    /**
     * @return array
     */
    public static function getSubscribingMethods()
    {
        return [
            array(
                'direction' => GraphNavigator::DIRECTION_DESERIALIZATION,
                'format' => 'json',
                'type' => Stop::class,
                'method' => 'deserializeField',
            ),
        ];
    }

    public function deserializeField(VisitorInterface $visitor, $data, array $type, Context $context)
    {
        $stops = new ArrayCollection();
        foreach ($data as $stopId => $stopName) {
            $stops[] = new Stop($stopId, $stopName);
        }
        return $stops;
    }
}