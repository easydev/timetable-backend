<?php

namespace spec\App\Timetable\Model;

use App\Timetable\Mode\Timetable;
use App\Timetable\Model\Line;
use App\Timetable\Model\Stop;
use App\Timetable\Model\Variant;
use App\Timetable\Model\VariantStop;
use Doctrine\Common\Collections\ArrayCollection;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class TimetableSpec extends ObjectBehavior
{
    function it_returns_timetable_for_line_id(
        Stop $stop1,
        Stop $stop2,
        Stop $stop3,
        Line $line,
        Variant $variant1,
        Variant $variant2,
        VariantStop $variantStop1,
        VariantStop $variantStop2,
        VariantStop $variantStop3,
        VariantStop $variantStop4,
        VariantStop $variantStop5,
        VariantStop $variantStop6
    ) {
        $stop1->beConstructedWith([31, 'test1']);
        $stop2->beConstructedWith([41, 'test2']);
        $stop3->beConstructedWith([51, 'test3']);
        $stops = new ArrayCollection([$stop1, $stop2, $stop3]);

        $variantStop1->getId()->willReturn(31);
        $variantStop2->getId()->willReturn(41);
        $variantStop3->getId()->willReturn(51);

        $variantStop4->getId()->willReturn(51);
        $variantStop5->getId()->willReturn(41);
        $variantStop6->getId()->willReturn(31);

        $variant1->beConstructedWith(['35', new ArrayCollection([$variantStop1, $variantStop2, $variantStop3])]);
        $variant2->beConstructedWith(['53', new ArrayCollection([$variantStop4, $variantStop5, $variantStop6])]);
        $line->getVariants()->willReturn([
            $variant1,
            $variant2,
        ]);

        $line->getDepartures()->willReturn(new ArrayCollection([
            '35' => new ArrayCollection(['01:10', '02:10', '03:10']),
            '53' => new ArrayCollection(['01:40', '02:40', '03:40']),
        ]));

        $lines = new ArrayCollection(['1' => $line]);

        $this->beConstructedWith($stops, $lines);

        $this->getStopTimetable(31)->shouldHaveCount(1);
    }
}
