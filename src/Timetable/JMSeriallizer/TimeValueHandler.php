<?php
namespace App\Timetable\JMSeriallizer;

use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\VisitorInterface;
use Vascowhite\Time\TimeValue;

class TimeValueHandler implements SubscribingHandlerInterface
{
    /**
     * @return array
     */
    public static function getSubscribingMethods()
    {
        return [
            array(
                'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
                'format' => 'json',
                'type' => TimeValue::class,
                'method' => 'deserializeField',
            ),
        ];
    }

    /**
     * @param VisitorInterface $visitor
     * @param TimeValue $data
     * @param array $type
     * @param Context $context
     * @return string
     */
    public function deserializeField(VisitorInterface $visitor, TimeValue $data, array $type, Context $context)
    {
        return $data->getTime();
    }
}