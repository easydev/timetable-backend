Feature: Timetable

  Scenario: Getting all stops
  When I send a GET request to "/"
  Then I get 200 status
  And the JSON should be equal to:
  """"
[
    {
        "id": 11,
        "stop_name": "Winna - Karpacka"
    },
    {
        "id": 21,
        "stop_name": "Piwna"
    },
    {
        "id": 31,
        "stop_name": "Rondo Tysi\u0105clecia"
    },
    {
        "id": 41,
        "stop_name": "Pi\u0105tkowicka - Wi\u0105czy\u0144ska"
    },
    {
        "id": 51,
        "stop_name": "Tacza\u0144ska"
    },
    {
        "id": 61,
        "stop_name": "Komarzewska"
    },
    {
        "id": 12,
        "stop_name": "Winna - Dzia\u0142ki"
    },
    {
        "id": 22,
        "stop_name": "Piwna - Karpacka"
    },
    {
        "id": 32,
        "stop_name": "Rondo Tysi\u0105clecia"
    },
    {
        "id": 42,
        "stop_name": "Pi\u0105tkowicka"
    },
    {
        "id": 52,
        "stop_name": "Tacza\u0144ska"
    },
    {
        "id": 62,
        "stop_name": "Komarzewska - Wi\u0105czy\u0144ska"
    }
]
  """"

  Scenario: Getting stop timetable
    When I send a GET request to "/stop/11"
    Then I get 200 status
    And the JSON should be equal to:
  """"
{
    "1": [
        "04:00:00",
        "04:50:00",
        "05:40:00",
        "06:30:00",
        "07:20:00",
        "08:10:00",
        "09:00:00",
        "10:00:00",
        "11:00:00",
        "12:00:00",
        "13:00:00",
        "14:00:00",
        "14:50:00",
        "15:40:00",
        "16:30:00",
        "17:20:00",
        "18:10:00",
        "19:00:00",
        "20:00:00",
        "21:00:00",
        "22:00:00"
    ]
}
  """"


  Scenario: Getting stop timetable
    When I send a GET request to "/departures/stop/11/4/00"
    Then I get 200 status
    And the JSON should be equal to:
  """"
[
    {
        "line": 1,
        "departure_time": "04:00:00",
        "human_departure_time": "due"
    },
    {
        "line": 1,
        "departure_time": "04:50:00",
        "human_departure_time": "50m"
    },
    {
        "line": 1,
        "departure_time": "05:40:00",
        "human_departure_time": "1h 40m"
    }
]
  """"